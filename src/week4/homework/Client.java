package week4.homework;

public final class Client {
	private static final String[] job = {
		"Сдать товар на ремонт",
		"Купить технику",
		"Забрать товар по идентификационному коду"
		};
	private String name;
	private int phone;
	private int[] serialNumbers;
	private int cashIn;
	public Client(String name, int phone, int[] serialNumbers, int cashIn) {
		super();
		this.name = name;
		this.phone = phone;
		this.serialNumbers = serialNumbers;
		this.cashIn = cashIn;
	}
	public static String[] getJob() {
		return job;
	}
	
}
