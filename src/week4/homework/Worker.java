package week4.homework;

import java.util.Date;

public abstract class Worker {
// данные работника: ФИО, адрес, профессия, дата приёма на работу.
	private String fio;
	private String addr;
	private String occupation;
	private Date dateOnJob;
// установка данных работника в конструкторе	
	public Worker(String fio, String addr, String occupation, Date dateOnJob) {
		super();
		this.fio = fio;
		this.addr = addr;
		this.occupation = occupation;
		this.dateOnJob = dateOnJob;
	}
// получение данных через геттеры
	public String getFio() {
		return fio;
	}
	public String getAddr() {
		return addr;
	}
	public String getOccupation() {
		return occupation;
	}
	public Date getDateOnJob() {
		return dateOnJob;
	}	
}
