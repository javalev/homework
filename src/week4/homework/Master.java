package week4.homework;

import java.util.Date;

public final class Master extends Worker{
	private static final String[] job = {
		"Ремонтировать",
		"Возвращать отремонтированную технику администратору" 
		};
	public Master(String fio, String addr, String occupation, Date dateOnJob) {
		super(fio, addr, occupation, dateOnJob);
	}
	public static String[] getJob() {
		return job;
	}
	
}
