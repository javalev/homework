package week4.homework;

import java.util.Date;

public final class Admin extends Worker{
	private static final String[] job = {
		"Посмотреть всех клиентов",
		"Показать отчет(количество отремонтированной техники за день, за неделю, за месяц)",
		"Взять на ремонт технику(цена ремонта = 10% от суммы товара)",
		"Отдать отремонтированный товар клиенту",
		"Передать технику специалисту по ремонту"
		};
	public Admin(String fio, String addr, String occupation, Date dateOnJob) {
		super(fio, addr, occupation, dateOnJob);
		// TODO Auto-generated constructor stub
	}
	public static String[] getJob() {
		return job;
	}
	
}
