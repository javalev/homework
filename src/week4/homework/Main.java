package week4.homework;

import java.util.Scanner;

public final class Main {

	public static void main(String[] args) {
		// открываем сервис: вводим имя, реальный адрес, сумма на р/с (касса).
Service service = new Service("Рога и копыта", "Багамские острова, д.34", 10000);		
		// нанимаем сотрудников: директора, админа, мастера: по 1-й тушке.
		service.init();	
		// войти в программу как директор, админ, мастер или клиент....
		Scanner sc = new Scanner(System.in);
		// выводим основное меню и выбираем пункт...
		int menu = Service.menuOutput(sc, Service.getMenu());
		int submenu = -1;
		// выводим подменю и выбираем кем хотим войти
		if (menu == 1) {
			submenu = Service.menuOutput(sc, Director.getJob());
			Director.jobRun(submenu);
		} else if (menu == 2){
			submenu = Service.menuOutput(sc, Admin.getJob());
		} else if (menu == 3){
			submenu = Service.menuOutput(sc, Client.getJob());
		} else if (menu == 4){
			submenu = Service.menuOutput(sc, Master.getJob());
		}
		
	}

}
