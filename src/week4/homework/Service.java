package week4.homework;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public final class Service {
	private static final String[] menu = {"Директор", "Админ", "Клиент", "Мастер"};
	private String serviceName;
	private String serviceAddr;
	private int kassa;

	private Director director;
	private Admin admin;
	private ArrayList<Master> master = new ArrayList<>();
	private ArrayList<Technic> technic = new ArrayList<>();

	public static String[] getMenu() {
		return menu;
	}
	public Service(String serviceName, String serviceAddr, int kassa) {
		super();
		this.serviceName = serviceName;
		this.serviceAddr = serviceAddr;
		this.kassa = kassa;
	}
	public void init(){
	director = new Director("Ляпис Трубецкой","ул.Мохнатая, д.34, кв.43","Директор", new Date());
	admin = new Admin("Алёна Блатная","пр.Дворников д.2, кв.123","Администратор", new Date());
	master.add(new Master("Вася Дуб","ул.Ленина д.20, кв.12","Мастер", new Date()));
	
	}
public static int menuOutput(Scanner sc, String[] Menu){
	System.out.println("Введите цифру для выбора меню или 0 для возврата в предыдущее:");
	int i = 1;
	for (String element : Menu) {
		System.out.println(i +" "+ element);
		i++;
		};
	return sc.nextInt();
	}	
}

