package week2.homework;

public class Number {
	private int value;

	// constructor to Int type
	public Number(int value) {
		this.value = value;
	}

	public Number add(Number argum) {
		return new Number(value + argum.value);
	}

	public Number sub(Number argum) {
		return new Number(value - argum.value);
	}

	public Number mul(Number argum) {
		return new Number(value * argum.value);
	}

	public Number div(Number argum) {
		return new Number(value / argum.value);
	}

	public Number exp(Number argum) {
		return new Number((int) Math.pow(value, argum.value));
	}

	public Number fact() {
		int ret = 1;
		for (int i = 1; i <= value; ++i)
			ret *= value;
		return new Number(ret);
	}

	public Number divPart(Number argum) {
		return new Number(value % argum.value);
	}

	public boolean bigger(Number argum) {
		if (value > argum.value)
			return true;
		return false;
	}

	public int toInt() {
		return this.value;
	}
}
