package week3.homework;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Director extends Employee {
	private int kassa;

	public Director(String fio, String addr, int yearsOld, int cash,
			Date onJob, int kassa) {
		super(fio, addr, yearsOld, cash, onJob);
		this.kassa = kassa;
	}

	public void ChangeServiceAddr(Scanner sc) {
		System.out.println(super.getAddr() + "- ������ ����� �����������");
		System.out.println("������� ����� ����� �����������:");
		sc.nextLine();
		super.setAddr(sc.nextLine());
		System.out.println(super.getAddr() + " - ����� ����� �����������");
		System.out.println();
	}

	public void emplOnJob(Scanner sc, ArrayList<Employee> workers) {
		System.out.println("��������� ������ ���������: ");
		System.out.println("������� ���: ");
		sc.nextLine();
		String fio = sc.nextLine();
		System.out.println("������� ����� ����������: ");
		String addr = sc.nextLine();
		// ������� �������� ������������� ��������
		int cash = 1800 + (int) (Math.random() * 4000);
		System.out.println("������� ��� ������� ");
		int yearOld = sc.nextInt();
		Employee newbie = new Employee(fio, addr, yearOld, cash, new Date());
		workers.add(newbie);
		System.out.println("�������� �������� � ���� � ����� ����:");
		System.out.println(workers.get(workers.size() - 1).getFio() + " "
				+ workers.get(workers.size() - 1).getYearsOld()
				+ " ��� ��������: " + workers.get(workers.size() - 1).getCash()
				+ "������ �." + workers.get(workers.size() - 1).getAddr()
				+ " ������ �� ������ �: "
				+ workers.get(workers.size() - 1).getOnJob() + " ");
	}

	public void dismissFromJob(Scanner sc, ArrayList<Employee> workers) {
		System.out.println("��������� ���������: ");
		System.out.println("������� ��� : "); // c ������ �������� ��� ����� �
												// ���� !!!
		sc.nextLine();
		String fio = sc.nextLine();
		int marker = -1;
		for (int i = 0; i < workers.size(); i++) {
			if (fio.equals(workers.get(i).getFio())) {
				System.out.println("������ �������� �� ����������, ��� �"
						+ (i + 1) + " ������� �� ������....");
				workers.remove(i);
				marker = 1;
			}
		}
		if (marker != 1)
			System.out.println("����������� ��������� �� ������ !");
		System.out.println();
	}

	public void viewAllEmployers(Scanner sc, ArrayList<Employee> workers) {

		System.out.println("������ ���������� � ����:");
		System.out.println();
		if (workers.size() == 0) {
			System.out.println("�������� ���������� � ���� !!!");
			System.out.println();
			return;
		}
		for (int i = 0; i < workers.size(); i++) {
			System.out.println(workers.get(i).getFio() + " "
					+ workers.get(i).getYearsOld() + " ��� ��������: "
					+ workers.get(i).getCash() + "������ �."
					+ workers.get(i).getAddr() + " ������ �� ������ �: "
					+ workers.get(i).getOnJob() + " ");
		}
		System.out.println();
	}

	public void payTaxes() {
		System.out.println("����� �� ������ �������: " + this.kassa
				+ " ����� 5% �� �����: " + 5 * (this.kassa / 100));
		// ������ ������ � ������� 5% �� �������
		this.kassa = this.kassa - 5 * (this.kassa / 100);
		System.out.println("������� � ����� ����� ������ �������: "
				+ this.kassa);
		System.out.println();
	}

	// prem - % ������������ ������� �������� �� 1 �� 400 ��������
	public void workersPrem(Scanner sc, ArrayList<Employee> workers) {
		System.out
				.println("������� ������� (1 - 200 )�� ������� % ������� �������� ���������� ?");
		int work = sc.nextInt();
		for (int i = 0; i < workers.size(); i++) {
			workers.get(i).setCash(
					workers.get(i).getCash() + (workers.get(i).getCash() / 100)
							* work);
		}
		System.out.println("�� ������, ��������� ��������� ����� ���� 4 ...");
		System.out.println();
	}
}
