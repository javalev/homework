package week3.homework;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// ����� ������������� �������
		ArrayList<Technic> inService = new ArrayList<Technic>(200);
		// ��������� �����������
		ArrayList<Employee> allWorkers = new ArrayList<Employee>(200);
		// ��������� �� ������ ���������.
		Director Hell = new Director("������ ����", "�������, ��� ������", 45,
				5000, new Date(), 1000);

		Scanner sc = new Scanner(System.in);
		// ������� ������ ���� (�������� ���� � ������� - ���� ArrayList)
		Menu menu = new Menu();
		// ������ ������ ���� � �������: 2 �����.
		boolean varMainMenu = true;
		boolean varSubMenu = true;
		while (varMainMenu) {
			menu.selectMainMenu(sc);
			varSubMenu = true;
			while (varSubMenu) {
				// ����� �������� ���. ����, ���� ����� ����� � �������� ����,
				// ��������� false
				if (!menu.selectSM(sc))
					break;
				// ���� ������� �������� ����(����������) - ��������� �������
				if (menu.getSelectMainMenu() == 1) {
					// �������� ����� ������ �������
					if (menu.getSelectSubMenu() == 1) {
						Hell.ChangeServiceAddr(sc);
						// ������� ��������� �� ������, ����� �� ������� ������
						// �� ��������� ������ ���������� !
					} else if (menu.getSelectSubMenu() == 2) {
						Hell.emplOnJob(sc, allWorkers);
						// �������� ��������� � ������
					} else if (menu.getSelectSubMenu() == 3) {
						Hell.dismissFromJob(sc, allWorkers);
						// ���������� ������ ���� �����������
					} else if (menu.getSelectSubMenu() == 4) {
						Hell.viewAllEmployers(sc, allWorkers);
						// �������� ������....
					} else if (menu.getSelectSubMenu() == 5) {
						Hell.payTaxes();
						// �������� ��������, ������ %
					} else if (menu.getSelectSubMenu() == 6) {
						Hell.workersPrem(sc, allWorkers);
					}
				}
			}
		}
	}
}
