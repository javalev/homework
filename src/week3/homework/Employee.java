package week3.homework;

import java.util.Date;

public class Employee {
	private String fio;
	private String addr;
	private int yearsOld;
	private int cash;
	private Date onJob;
	
public Employee(String fio, String addr,int cash){
	this.addr = addr;
	this.fio = fio;
	this.cash = cash;
}
public Employee(String fio, String addr, int yearsOld, int cash, Date onJob){
	this.addr = addr;
	this.fio = fio;
	this.yearsOld = yearsOld;
	this.cash = cash;
	this.onJob = onJob;
}
public String getFio() {
	return fio;
}
public void setFio(String fio) {
	this.fio = fio;
}
public String getAddr() {
	return addr;
}
public void setAddr(String addr) {
	this.addr = addr;
}
public int getYearsOld() {
	return yearsOld;
}
public void setYearsOld(int yearsOld) {
	this.yearsOld = yearsOld;
}
public int getCash() {
	return cash;
}
public void setCash(int cash) {
	this.cash = cash;
}
public Date getOnJob() {
	return onJob;
}
public void setOnJob(Date onJob) {
	this.onJob = onJob;
}
}
