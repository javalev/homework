package week1.homework;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		// init: name, address, workers, generate list of workers
		Company sever = new Company();
		sever.initCompanyInfo();
		// init scanner and menu
		Scanner sc = new Scanner(System.in);
		Menu menu = new Menu();
		// infinity cycle for menu
		int index = 1;
		while (index > 0) {
			// print all menu in loop
			for (int i = 0; i < menu.DataOfMenu.length; i++) {
				System.out.println(menu.DataOfMenu[i]);
			}
			// wait input from keyboard
			System.out.println();
			System.out
					.println(" ��� ����� �������� ������ ���� (�� 1 �� 10): ");
			int menuInputData = sc.nextInt(); // input number menu
			int ind = 0;
			if (menuInputData == 1) {
				// view company info
				sever.outputCompanyInfo();
				// view full list of Company employers
			} else if (menuInputData == 2) {
				ind = 0;
				for (ind = 0; ind < sever.workersAll; ind++) {
					if (sever.list[ind].fio != "not filled")
						sever.advOutputWorker(sever.list, ind);
				}
				System.out.println();
				// view all Employers, who work > 1 year.
			} else if (menuInputData == 3) {
				ind = 0;
				while (ind < sever.workersAll) {
					if (sever.list[ind].monthInCompany > 12)
						sever.advOutputWorker(sever.list, ind);
					ind++;
				}
				// print on console all females work in Kiev
			} else if (menuInputData == 4) {
				ind = 0;
				while (ind < sever.workersAll) {
					if (sever.list[ind].gender == "����"
							&& sever.list[ind].addr.town == "����")
						sever.advOutputWorker(sever.list, ind);
					ind++;
				}
				// add employer to Company list
			} else if (menuInputData == 5) {
				for (int i = 0; i < sever.workersAll; i++) {
					if (sever.list[i].fio == "not filled") {
						sever.list[i].generateEmployer();
						System.out.println("�������� �������� ��� ������� = "
								+ (i + 1));
						break;
					}
				}
				// discharge last in list employer
			} else if (menuInputData == 6) {
				for (int i = 0; i < sever.workersAll; i++) {
					if (sever.list[i].fio == "not filled") {
						sever.fillClearEmployer(i - 1);
						break;
					}
				}
				// discharge employer with cashIn < 1000 and < 12 month in
				// company
			} else if (menuInputData == 7) {
				for (int i = 0; i < sever.workersAll; i++) {
					if (sever.list[i].fio == "not filled")
						continue;
					if (sever.list[i].cashWork < 1000) {
						if (sever.list[i].monthInCompany < 12) {
							sever.fillClearEmployer(i);
							System.out.println("������ �������� � " + (i + 1)
									+ "c �� < 1000 � ������� < 12���. ");
							System.out.println();
						}
					}
				}
				// change data of employer
			} else if (menuInputData == 8) {
				System.out
						.println("������� � ���������, � �������� ��������� �������� ������:");
				ind = sc.nextInt();
				System.out.println("������� ������� ��� �������� ����� ������");
				sever.list[ind].fio = sc.toString();

				// first print list of male, then list of female employers
			} else if (menuInputData == 9) {
				for (ind = 0; ind < sever.workersAll; ind++) {
					if (sever.list[ind].gender == "�����")
						sever.advOutputWorker(sever.list, ind);
				}
				for (ind = 0; ind < sever.workersAll; ind++) {
					if (sever.list[ind].gender == "����")
						sever.advOutputWorker(sever.list, ind);
				}
				// view employers who working from 100 to 200 hours
			} else if (menuInputData == 10) {

			}
		}

	}
}
